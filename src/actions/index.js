import axios from "axios";
axios.defaults.withCredentials = true;

const ROOT_URL = `http://localhost:9292/cars`;

export const FETCH_CAR = "FETCH_CAR";

export function fetchWeather(brand) {
  const url = `${ROOT_URL}?brand=${brand}`;
  const request = axios.get(url, {
    withCredentials: true,
    headers: { 
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Token token=5a6afda262796fec3cca2fabaad88c9a'}
  });

  console.log(request)
  return {
    type: FETCH_CAR,
    payload: request
  };
}
