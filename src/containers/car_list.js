import React, { Component } from "react";
import { connect } from "react-redux";

class WeatherList extends Component {
  renderCars() {
    console.log(this.props.cars)

    return this.props.cars.map((car) => {
      console.log(this.props.cars)
      return (
        <tr key={car.id}>
          <td>{car.brand}</td>
          <td>{car.model}</td>
          <td>{car.price}</td>
          <td>{car.year}</td>
        </tr>
      );
    });
    
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Ano</th>
            <th>Preço</th>
          </tr>
        </thead>
        <tbody>
          {this.renderCars()}
        </tbody>
      </table>
    );
  }
}

function mapStateToProps({ cars }) {
  return { cars };
}

export default connect(mapStateToProps)(WeatherList);
