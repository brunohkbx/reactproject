import { FETCH_CAR } from "../actions/index";

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_CAR:
      return action.payload.data
  }
  return state;
}
