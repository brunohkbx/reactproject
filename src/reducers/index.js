import { combineReducers } from "redux";
import carReducer from "./reducer_car";

const rootReducer = combineReducers({
  cars: carReducer
});

export default rootReducer;
